package co.wellsen.customsmsgateway;

public class PayloadData {

  String id;
  String recipient;
  String message;
  long requestedTime;
  String webhookUrl;

  public PayloadData() {
    this("", "", "", 0, "");
  }

  public PayloadData(String id, String recipient, String message, long requestedTime, String webhookUrl) {
    this.id = id;
    this.recipient = recipient;
    this.message = message;
    this.requestedTime = requestedTime;
    this.webhookUrl = webhookUrl;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public long getRequestedTime() {
    return requestedTime;
  }

  public void setRequestedTime(long requestedTime) {
    this.requestedTime = requestedTime;
  }

  public String getWebhookUrl() {
    return webhookUrl;
  }

  public void setWebhookUrl(String webhookUrl) {
    this.webhookUrl = webhookUrl;
  }
}
