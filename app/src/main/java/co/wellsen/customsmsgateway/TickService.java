package co.wellsen.customsmsgateway;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import java.util.Timer;
import java.util.TimerTask;

public class TickService extends Service {

  private static final String LOG_TAG = TickService.class.getCanonicalName();

  public static final String START_FOREGROUND_ACTION = "start_foreground_Action";
  public static final String STOP_FOREGROUND_ACTION = "stop_foreground_action";

  public static final int FOREGROUND_SERVICE_NOTIFICATION_ID = 101;
  private static final int REFRESH_TIME_MILLIS = 2;   // ONE SECOND

  private CustomSmsGateway app;
  private Timer timer;

  private long delay = REFRESH_TIME_MILLIS;

  @Override
  public void onCreate() {
    super.onCreate();

    app = (CustomSmsGateway) this.getApplication();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (intent == null) {
      return START_STICKY;
    }

    if (intent.getAction().equals(START_FOREGROUND_ACTION)) {
      showNotification();

      app.allowedToGetRequest = true;

      app.setRun(true);

      SharedPreferences pref = getSharedPreferences(CustomSmsGateway.PREF_NAME, MODE_PRIVATE);
      pref.edit().putBoolean("isRun", true).apply();

      delay = SharedPreferenceHelper.getDelay();

      timer = new Timer("timer", true);
      timer.scheduleAtFixedRate(timerTask, delay * 1000, delay * 1000);

      app.doLog("Service Started");

    } else if (intent.getAction().equals(STOP_FOREGROUND_ACTION)) {
      timer.cancel();
      timer = null;

      stopForeground(true);
      stopSelf();

      app.setRun(false);

      SharedPreferences pref = getSharedPreferences(CustomSmsGateway.PREF_NAME, MODE_PRIVATE);
      pref.edit().putBoolean("isRun", false).apply();

      app.doLog("Service Stopped");
    }

    Intent itn = new Intent(MainActivity.FILTER_REFRESH_BUTTON_STATE);
    sendBroadcast(itn);

    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    app.setRun(false);

    SharedPreferences pref = getSharedPreferences(CustomSmsGateway.PREF_NAME, MODE_PRIVATE);
    pref.edit().putBoolean("isRun", false).apply();

    if (timer != null) {
      timer.cancel();
      timer = null;
    }

    super.onDestroy();
  }

  private void showNotification() {
    Intent notificationIntent = new Intent(this, MainActivity.class);
    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

    Notification notification = new NotificationCompat.Builder(this)
        .setContentTitle("Voxidroid")
        .setTicker("Voxidroid is running")
        .setContentText("Running")
        .setSmallIcon(R.mipmap.ic_launcher)
        .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
        .setContentIntent(pendingIntent)
        .setOngoing(true)
        .build();

    startForeground(FOREGROUND_SERVICE_NOTIFICATION_ID, notification);
  }

  //--

  private TimerTask timerTask = new TimerTask() {
    @Override
    public void run() {
      if (app.allowedToGetRequest) {
        try {
          app.getData();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  };
}
