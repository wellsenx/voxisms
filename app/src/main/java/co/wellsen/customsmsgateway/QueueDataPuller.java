package co.wellsen.customsmsgateway;

import android.content.Context;
import android.util.Log;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import java.util.List;

public class QueueDataPuller {

  private static final String TAG = QueueDataPuller.class.getSimpleName();

  private static final String ACCESS_KEY = "AKIAIOK4HPYRQ3DUEV2Q";
  private static final String SECRET_KEY = "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";

  /* SQS queue URL */
  private String queueURL = "https://sqs.us-east-1.amazonaws.com/394885935489/sms.fifo";

  /* SQS queue */
  private AmazonSQS sqs;

  /* Current temperature */
  private String currentTemperature;

  /* Caching file name */
  public static final String TEMP_CACHE_NAME = "TemperatureCacheFile";

  /* Caching string */
  public static final String cachedTemperature = "temperature";

  public QueueDataPuller(Context appContext) {
    // TODO: Gather the cognito credentials from the User rather than initializing deep inside the config
    // Initialize the Amazon Cognito credentials provider

    BasicAWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

    // Initialize sqs client
    sqs = new AmazonSQSClient(credentials);
    sqs.setRegion(Region.getRegion(Regions.US_EAST_1));

    Log.i(TAG, "Initialized SQS Client with Basic credentials");
  }

  public String getMessage(Context appContext) {
    String strMessage = "";

    ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueURL);
    List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
    Log.i(TAG, "Received messages from queue");

    if (!messages.isEmpty()) {
      // Show only the first message. Queue is always FIFO.
      Message message = messages.get(0);

      strMessage = message.getBody();
      Log.d(TAG, "Body of the message: " + strMessage);

      //String messageReceiptHandle = message.getReceiptHandle();

      //sqs.deleteMessage(new DeleteMessageRequest(queueURL, messageReceiptHandle));
      //Log.i(TAG, "Deleting message: " + messageReceiptHandle);
    } else {
      Log.d(TAG, "Queue is empty");
    }

    return strMessage;
  }
}