package co.wellsen.customsmsgateway;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import io.fabric.sdk.android.Fabric;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
//import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Project CustomSMSGateway.
 *
 * Created by wellsen on 08/02/18.
 */
public class CustomSmsGateway extends Application {

  private static final String TAG = CustomSmsGateway.class.getCanonicalName();

  public static final String PREF_NAME = "Voxidroid";

  private final String KEY_SENT = "SMS_SENT";
  private final String KEY_DELIVERY = "SMS_DELIVERED";

  public static final String ACTION_SEND_SMS = "send_sms";
  public static final String KEY_ADDRESS = "address";
  public static final String KEY_MESSAGE = "message";
  public static final String KEY_WEBHOOK_URL = "webhookUrl";

  public static final String KEY_POST_DATA = "keyPostData";

  private boolean isRun;
  public boolean allowedToGetRequest;

  private static CustomSmsGateway instance;

  //--

  private static final String ACCESS_KEY = "AKIAIOK4HPYRQ3DUEV2Q";
  private static final String SECRET_KEY = "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";

  private String queueURL = "https://sqs.us-east-1.amazonaws.com/394885935489/sms.fifo";

  private AmazonSQS sqs;

  @Override
  public void onCreate() {
    super.onCreate();
    Fabric.with(this, new Crashlytics(), new Answers());

    doStartService();

    instance = this;
  }

  public void doStartService() {
    Intent itn = new Intent(this, TickService.class);
    itn.setAction(TickService.START_FOREGROUND_ACTION);
    startService(itn);

    registerReceiver(smsReceiver, new IntentFilter(ACTION_SEND_SMS));
    registerReceiver(sentReceiver, new IntentFilter(KEY_SENT));
    registerReceiver(deliveryReceiver, new IntentFilter(KEY_DELIVERY));
  }

  public void doStopService() {
    Intent itn = new Intent(this, TickService.class);
    itn.setAction(TickService.STOP_FOREGROUND_ACTION);
    startService(itn);

    try {
      unregisterReceiver(sentReceiver);
      unregisterReceiver(deliveryReceiver);
      unregisterReceiver(smsReceiver);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onTerminate() {
    doStopService();

    super.onTerminate();
  }

  //--

  public static CustomSmsGateway getInstance() {
    return instance;
  }

  public boolean isRun() {
    return isRun;
  }

  public void setRun(boolean run) {
    isRun = run;
  }

  //--

  public void getData() throws Exception {
    allowedToGetRequest = false;

    BasicAWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

    // Initialize sqs client
    sqs = new AmazonSQSClient(credentials);
    sqs.setRegion(Region.getRegion(Regions.US_EAST_1));

    String strMessage = "";

    ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueURL);
    List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

    if (!messages.isEmpty()) {
      doLog("Received messages from queue");
    }

    if (!messages.isEmpty()) {
      // Show only the first message. Queue is always FIFO.
      Message message = messages.get(0);

      strMessage = message.getBody();
      doLog("Body of the received message (from queue): " + strMessage);

      PayloadData payloadData = null;

      try {
        payloadData = new Gson().fromJson(strMessage, PayloadData.class);
      } catch (JsonSyntaxException e) {
        e.printStackTrace();
      }

      String messageReceiptHandle = message.getReceiptHandle();

      try {
        sqs.deleteMessage(new DeleteMessageRequest(queueURL, messageReceiptHandle));
        doLog("Deleting message: " + messageReceiptHandle);
      } catch (AmazonServiceException e) {
        e.printStackTrace();
        allowedToGetRequest = true;
        return;
      }

      if (payloadData == null) {
        allowedToGetRequest = true;
        return;
      }

      String destination = payloadData.getRecipient();
      String msg = payloadData.getMessage();

      if (destination != null && !destination.trim().isEmpty() && msg != null && !msg.trim().isEmpty()) {
        sendSms(destination, msg, payloadData);
      }
    } else {
      //doLog("Queue is empty");
    }

    allowedToGetRequest = true;
  }

  //--

  public void sendSms(String destination, String message, PayloadData payloadData) {
    doLog("Sending sms to: " + destination + " with message content: " + message);

    Intent sentIntent = new Intent(KEY_SENT);
    sentIntent.putExtra(KEY_ADDRESS, destination);

    Intent deliveryIntent = new Intent(KEY_DELIVERY);
    deliveryIntent.putExtra(KEY_ADDRESS, destination);

    if (payloadData != null) {
      PostData postData = new PostData();

      if (payloadData.getId() != null && !payloadData.getId().trim().isEmpty()) {
        postData.setId(payloadData.getId());
      }

      if (payloadData.getRecipient() != null && !payloadData.getRecipient().trim().isEmpty()) {
        postData.setRecipient(payloadData.getRecipient());
      }

      if (payloadData.getMessage() != null && !payloadData.getMessage().trim().isEmpty()) {
        postData.setMessage(payloadData.getMessage());
      }

      postData.setTime(payloadData.getRequestedTime());

      sentIntent.putExtra( KEY_POST_DATA, new Gson().toJson(postData));
      deliveryIntent.putExtra(KEY_POST_DATA, new Gson().toJson(postData));

      if (payloadData.getWebhookUrl() != null && !payloadData.getWebhookUrl().trim().isEmpty()) {
        sentIntent.putExtra(KEY_WEBHOOK_URL, payloadData.getWebhookUrl());
        deliveryIntent.putExtra(KEY_WEBHOOK_URL, payloadData.getWebhookUrl());
      }
    }

    PendingIntent sentPI = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), sentIntent, 0);
    PendingIntent deliveredPI = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(),deliveryIntent, 0);

    //--

    SmsManager sms = SmsManager.getDefault();
    sms.sendTextMessage(destination, null, message, sentPI, deliveredPI);
  }

  private BroadcastReceiver smsReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String destination = intent.getStringExtra(KEY_ADDRESS);
      String message = intent.getStringExtra(KEY_MESSAGE);

      sendSms(destination, message, null);
    }
  };

  private BroadcastReceiver sentReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      //PostData postData = (PostData) intent.getSerializableExtra(KEY_POST_DATA);
      String sPostData = intent.getStringExtra(KEY_POST_DATA);
      PostData postData = new Gson().fromJson(sPostData, PostData.class);

      String webhookUrl = intent.getStringExtra(KEY_WEBHOOK_URL);

      if (postData != null && webhookUrl != null && webhookUrl.trim().length() > 0) {
        postData.setType(1);

        switch (getResultCode()) {
          case Activity.RESULT_OK:
            postData.setStatus(-1);
            postData.setStatusText("SMS sent");
            break;

          case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
            postData.setStatus(1);
            postData.setStatusText("SMS Sending Failed: Generic failure");
            break;

          case SmsManager.RESULT_ERROR_NO_SERVICE:
            postData.setStatus(4);
            postData.setStatusText("SMS Sending Failed: Service is currently unavailable");
            break;

          case SmsManager.RESULT_ERROR_NULL_PDU:
            postData.setStatus(3);
            postData.setStatusText("SMS Sending Failed: No PDU provided");
            break;

          case SmsManager.RESULT_ERROR_RADIO_OFF:
            postData.setStatus(2);
            postData.setStatusText("SMS Sending Failed: Radio was turned off");
            break;

          default:
            break;
        }

        new HttpPostRequestTask(webhookUrl, postData).execute(null, null, null);
      }
    }
  };

  private BroadcastReceiver deliveryReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      //PostData postData = (PostData) intent.getSerializableExtra(KEY_POST_DATA);
      String sPostData = intent.getStringExtra(KEY_POST_DATA);
      PostData postData = new Gson().fromJson(sPostData, PostData.class);

      String webhookUrl = intent.getStringExtra(KEY_WEBHOOK_URL);

      if (postData != null && webhookUrl != null && webhookUrl.trim().length() > 0) {
        postData.setType(2);

        switch (getResultCode()) {
          case Activity.RESULT_OK:
            postData.setStatus(9);
            postData.setStatusText("SMS Delivered");
            break;

          case Activity.RESULT_CANCELED:
            postData.setStatus(10);
            postData.setStatusText("SMS Delivery Failed");
            break;
        }

        new HttpPostRequestTask(webhookUrl, postData).execute(null, null, null);
      }
    }
  };

  //--

  private class HttpPostRequestTask extends AsyncTask<Void, Void, Void> {

    private String url;
    private PostData postData;

    private HttpPostRequestTask() {

    }

    public HttpPostRequestTask(String url, PostData postData) {
      this.url = url;
      this.postData = postData;
    }

    @Override
    protected Void doInBackground(Void... voids) {

      performPostCall();

      return null;
    }

    public String  performPostCall() {
      doLog("HttpPostRequestTask -> id: " + postData.getId() + ", status: " + postData.getStatus() + ", statusText: " + postData.getStatusText());

      URL url;
      String response = "";
      try {
        url = new URL(this.url);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        Gson gson = new Gson();

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(gson.toJson(postData));

        writer.flush();
        writer.close();
        os.close();
        int responseCode=conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
          String line;
          BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
          while ((line=br.readLine()) != null) {
            response+=line;
          }
        }
        else {
          response="";

        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      return response;
    }
  }

  public void doLog(String logMessage) {
    Log.d(TAG, logMessage);

    Intent intent = new Intent(MainActivity.FILTER_LOG_MESSAGE);
    if (isRun) {
      intent.putExtra(MainActivity.KEY_LOG_MESSAGE, logMessage);
    } else {
      intent.putExtra(MainActivity.KEY_LOG_MESSAGE, "Service Stopped");
      Log.d(TAG, "Service Stopped");
    }
    sendBroadcast(intent);
  }
}
