package co.wellsen.customsmsgateway;

import java.io.Serializable;

public class PostData implements Serializable {

  String id;
  int type;             //
  String recipient;
  String message;
  long time;
  int status;           //
  String statusText;    //

  public PostData() {
    this("", "", "", 0, 0, "");
  }

  public PostData(String id, String recipient, String message, long time, int status, String statusText) {
    this.id = id;
    this.recipient = recipient;
    this.message = message;
    this.time = time;
    this.status = status;
    this.statusText = statusText;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getStatusText() {
    return statusText;
  }

  public void setStatusText(String statusText) {
    this.statusText = statusText;
  }
}
