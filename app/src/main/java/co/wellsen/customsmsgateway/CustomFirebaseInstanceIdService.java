/*
package co.wellsen.customsmsgateway;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

*/
/**
 * Project CustomSMSGateway.
 *
 * Created by wellsen on 08/02/18.
 *//*

public class CustomFirebaseInstanceIdService extends FirebaseInstanceIdService {

  private static final String LOG_TAG = CustomFirebaseInstanceIdService.class.getCanonicalName();

  */
/**
   * Called if InstanceID token is updated. This may occur if the security of
   * the previous token had been compromised. Note that this is called when the InstanceID token
   * is initially generated so this is where you would retrieve the token.
   *//*

  @Override
  public void onTokenRefresh() {
    // Get updated InstanceID token.
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.i(LOG_TAG, "onTokenRefresh; Firebase Token: " + refreshedToken);

    // If you want to send messages to this application instance or
    // manage this apps subscriptions on the server side, send the
    // Instance ID token to your app server.
    //sendRegistrationToServer(refreshedToken);

  }

}
*/
