package co.wellsen.customsmsgateway;

import android.content.Context;

public class SharedPreferenceHelper {

  private static final String APPLICATION = "voxidroid";

  private static final String KEY_DELAY = "keyDelay";

  public static void setDelay(long delay) {
    CustomSmsGateway.getInstance().getSharedPreferences(APPLICATION, Context.MODE_PRIVATE)
        .edit()
        .putLong(KEY_DELAY, delay);
  }

  public static long getDelay() {
    return CustomSmsGateway.getInstance().getSharedPreferences(APPLICATION, Context.MODE_PRIVATE).getLong(KEY_DELAY, 2);
  }
}
