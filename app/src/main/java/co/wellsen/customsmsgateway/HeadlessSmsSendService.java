package co.wellsen.customsmsgateway;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Project CustomSMSGateway.
 *
 * Created by wellsen on 15/02/18.
 */

public class HeadlessSmsSendService extends Service {

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

}
