package co.wellsen.customsmsgateway;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
//import com.google.firebase.iid.FirebaseInstanceId;
import java.util.Locale;

@SuppressWarnings({"FieldCanBeLocal"})
public class MainActivity extends AppCompatActivity {

  private static final String LOG_TAG = MainActivity.class.getCanonicalName();

  public static final String FILTER_LOG_MESSAGE = "filterLogMessage";
  public static final String KEY_LOG_MESSAGE = "keyLogMessage";

  public static final String FILTER_REFRESH_BUTTON_STATE = "filterRefreshButtonState";

  /**
   * Id to identify a contacts permission request.
   */
  private static final int REQUEST_PERMISSIONS = 1;

  private LinearLayout ll;

  private EditText etDelay;
  private View vwDisableInput;
  private Button btnChangeSave;

  private TextView tvLog;
  private Button btnStartStop;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setupViews();

    requestPermissionsAndStart();
  }

  @Override
  protected void onResume() {
    super.onResume();

    registerReceiver(refreshButtonStateFilter, new IntentFilter(FILTER_REFRESH_BUTTON_STATE));
    registerReceiver(logReceiver, new IntentFilter(FILTER_LOG_MESSAGE));

    checkDefaultSmsApp();
    checkStatusService();
  }

  @Override
  protected void onPause() {
    super.onPause();

    unregisterReceiver(refreshButtonStateFilter);
    unregisterReceiver(logReceiver);
  }

  private void setupViews() {
    setContentView(R.layout.activity_main);

    ll = findViewById(R.id.ll);

    etDelay = (EditText) findViewById(R.id.etDelay);
    vwDisableInput = findViewById(R.id.vwDisableInput);
    btnChangeSave = findViewById(R.id.btnChangeSave);

    btnChangeSave.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        doChangeSaveInput();
      }
    });

    tvLog = (TextView) findViewById(R.id.tvLog);
    btnStartStop = (Button) findViewById(R.id.btnStartStop);

    btnStartStop.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        doStartStop();
      }
    });

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
  }

  private void checkStatusService() {
    long delay = SharedPreferenceHelper.getDelay();
    etDelay.setText("" + delay);

    if (vwDisableInput.getVisibility() == View.VISIBLE) {
      btnChangeSave.setText("Change");
      btnChangeSave.setEnabled(true);
    } else {
      btnChangeSave.setText("Save");
      btnChangeSave.setEnabled(false);
    }

    if (CustomSmsGateway.getInstance().isRun()) {
      btnChangeSave.setEnabled(false);
      btnStartStop.setText(getString(R.string.label_stop));
    } else {
      btnChangeSave.setEnabled(true);
      btnStartStop.setText(getString(R.string.label_start));
    }
  }

  private void doChangeSaveInput() {
    if (CustomSmsGateway.getInstance().isRun()) {
      vwDisableInput.setVisibility(View.VISIBLE);
      btnChangeSave.setEnabled(false);
    } else {
      if (vwDisableInput.getVisibility() == View.VISIBLE) {
        vwDisableInput.setVisibility(View.GONE);
        btnChangeSave.setText("Save");
        btnStartStop.setEnabled(false);
      } else {
        if (etDelay.getText().toString().length() > 0) {
          String sNumber = etDelay.getText().toString();
          try {
            int delay = Integer.parseInt(sNumber);

            if (delay > 2) {
              if (delay < 3600) {

              } else {
                delay = 3600;
              }
            } else {
              delay = 2;
            }

            SharedPreferenceHelper.setDelay(delay);
            etDelay.setText("" + delay);
            vwDisableInput.setVisibility(View.VISIBLE);
            btnChangeSave.setText("Change");
            btnStartStop.setEnabled(true);
          } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
          }
        }
      }
    }
  }

  private void doStartStop() {
    if (CustomSmsGateway.getInstance().isRun()) {
      CustomSmsGateway.getInstance().doStopService();
    } else {
      CustomSmsGateway.getInstance().doStartService();
    }
  }

  private void requestPermissionsAndStart() {
    // Check if the all permissions needed already available.
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        != PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
        != PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
        != PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
        != PackageManager.PERMISSION_GRANTED) {

      // One or more permission/s has not been granted.
      requestPermissions();

    } else {
      // All permissions needed already available, start the app.
      start();
    }
  }

  /**
   * Requests the all permissions needed.
   * If the permission has been denied previously, a SnackBar will prompt the user to grant the
   * permission, otherwise it is requested directly.
   */
  private void requestPermissions() {
    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.READ_PHONE_STATE)
        || ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.SEND_SMS)
        || ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.RECEIVE_SMS)
        || ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.READ_SMS)) {

      // Provide an additional rationale to the user if the permission/s was not granted
      // and the user would benefit from additional context for the use of the permission.
      // For example if the user has previously denied the permission.
      Snackbar.make(ll, "Need this permission to run",
          Snackbar.LENGTH_INDEFINITE)
          .setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              ActivityCompat.requestPermissions(MainActivity.this,
                  new String[] {Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS,
                      Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS},
                  REQUEST_PERMISSIONS);
            }
          })
          .show();

    } else {
      // Permission/s has not been granted yet. Request it directly.
      ActivityCompat.requestPermissions(this, new String[] {
          Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS,
              Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS},
          REQUEST_PERMISSIONS);
    }
  }

  /**
   * Callback received when a permissions request has been completed.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {

    if (requestCode == REQUEST_PERMISSIONS) {
      Log.i(LOG_TAG, "Received response for permission/s request.");

      // We have requested multiple permissions, so all of them need to be checked.
      if (PermissionUtil.verifyPermissions(grantResults)) {
        // All required permissions have been granted, start the app.
        start();

      } else {
        Log.i(LOG_TAG, "Permission/s were NOT granted, exiting App");
        finish();
      }

    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void checkDefaultSmsApp() {
    final String myPackageName = getPackageName();
    if (Telephony.Sms.getDefaultSmsPackage(this).equals(myPackageName)) {
      // App is default, proceed.
      return;
    }

    // App is not default.
    // Show the "not currently set as the default SMS app" interface
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        switch (which){
          case DialogInterface.BUTTON_POSITIVE:
            //Yes button clicked
            Intent intent =
                new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME,
                myPackageName);
            startActivity(intent);
            break;

          case DialogInterface.BUTTON_NEGATIVE:
            //No button clicked
            finish();
            break;

          default:
            finish();
            break;
        }
      }
    };

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("Custom SMS Gateway must be set as default messaging app")
        .setPositiveButton("Yes", dialogClickListener)
        .setNegativeButton("No", dialogClickListener)
        .show();
  }

  private void start() {
    Log.i(LOG_TAG, "Starting the app");
  }

  //--
  private BroadcastReceiver logReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String logMessage = intent.getStringExtra(KEY_LOG_MESSAGE);
      if (logMessage != null && logMessage.trim().length() > 0) {
        tvLog.setText(logMessage);
      }
    }
  };

  private BroadcastReceiver refreshButtonStateFilter = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      checkStatusService();
    }
  };
}
